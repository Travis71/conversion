package GUI;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class DepreIFrame extends javax.swing.JInternalFrame {

    private String strType;
    private int intType;
    public static int DEPR_LINEAL = 0;
    public static int DEPR_SDAA = 1;
    public static int DEPR_SDAD = 2;
    private int anos;
    
    public DepreIFrame(int type) {
        this.intType = type;
        setType();
        initComponents();
    }
    
    private void setType(){
        switch (this.intType){
            case 0: this.strType = "Lineal";
            break;
            case 1: this.strType = "SDA ascendente";
            break;
            case 2: this.strType = "SDA descendente";
            break;
        }
    }
    
    private void updateTable(){
        String[] names = new String[this.anos+1];
        for (int i = 1; i <= this.anos; i++){
            names[i-1] = "Año "+i;
        }
        names[names.length-1] = "Total";
        TableModel temp = new DefaultTableModel(names, 1);
        jTable1.setModel(temp);
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        BottomPanel = new javax.swing.JPanel();
        CalcButton = new javax.swing.JButton();
        CancelButton = new javax.swing.JButton();
        CenterPanel = new javax.swing.JPanel();
        vaLabel = new javax.swing.JLabel();
        vaFTF = new javax.swing.JFormattedTextField();
        vrLabel = new javax.swing.JLabel();
        vrFTF = new javax.swing.JFormattedTextField();
        tpLabel = new javax.swing.JLabel();
        tpCB = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Depreciación "+strType);
        setMinimumSize(new java.awt.Dimension(600, 525));

        CalcButton.setText("Calcular");
        BottomPanel.add(CalcButton);

        CancelButton.setText("Cancelar");
        BottomPanel.add(CancelButton);

        getContentPane().add(BottomPanel, java.awt.BorderLayout.PAGE_END);

        CenterPanel.setMinimumSize(new java.awt.Dimension(600, 525));
        CenterPanel.setPreferredSize(new java.awt.Dimension(600, 525));
        java.awt.GridBagLayout CenterPanelLayout = new java.awt.GridBagLayout();
        CenterPanelLayout.columnWidths = new int[] {0, 10, 0};
        CenterPanelLayout.rowHeights = new int[] {0, 3, 0, 3, 0, 3, 0};
        CenterPanel.setLayout(CenterPanelLayout);

        vaLabel.setText("Valor del Activo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 50, 0, 0);
        CenterPanel.add(vaLabel, gridBagConstraints);

        vaFTF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        CenterPanel.add(vaFTF, gridBagConstraints);

        vrLabel.setText("Valor Residual");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        CenterPanel.add(vrLabel, gridBagConstraints);

        vrFTF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        CenterPanel.add(vrFTF, gridBagConstraints);

        tpLabel.setText("Tipo de Activo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        CenterPanel.add(tpLabel, gridBagConstraints);

        tpCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Vehiculo", "Computadora", "Edificio", "Maquinarias", "Bienes Muebles" }));
        tpCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tpCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        CenterPanel.add(tpCB, gridBagConstraints);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 100;
        CenterPanel.add(jScrollPane1, gridBagConstraints);

        getContentPane().add(CenterPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tpCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tpCBActionPerformed
        switch (tpCB.getSelectedIndex()){
            case 0: this.anos = 5;
            break;
            case 1: this.anos = 2;
            break;
            case 2: this.anos = 20;
            break;
            case 3: this.anos = 10;
            break;
            case 4: this.anos = 5;
            break;
        }
        updateTable();
    }//GEN-LAST:event_tpCBActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BottomPanel;
    private javax.swing.JButton CalcButton;
    private javax.swing.JButton CancelButton;
    private javax.swing.JPanel CenterPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JComboBox<String> tpCB;
    private javax.swing.JLabel tpLabel;
    private javax.swing.JFormattedTextField vaFTF;
    private javax.swing.JLabel vaLabel;
    private javax.swing.JFormattedTextField vrFTF;
    private javax.swing.JLabel vrLabel;
    // End of variables declaration//GEN-END:variables
}
