package GUI;

import javax.swing.JFrame;

public class mainFrame extends JFrame {

    public mainFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dskpPane = new javax.swing.JDesktopPane();
        menuBar = new javax.swing.JMenuBar();
        convMenu = new javax.swing.JMenu();
        tempIMenu = new javax.swing.JMenuItem();
        longIMenu = new javax.swing.JMenuItem();
        pesoIMenu = new javax.swing.JMenuItem();
        contMenu = new javax.swing.JMenu();
        deprIMenu = new javax.swing.JMenu();
        lineIMenu2 = new javax.swing.JMenuItem();
        SDAaIMenu2 = new javax.swing.JMenuItem();
        SDAdIMenu2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Conversiones");
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        javax.swing.GroupLayout dskpPaneLayout = new javax.swing.GroupLayout(dskpPane);
        dskpPane.setLayout(dskpPaneLayout);
        dskpPaneLayout.setHorizontalGroup(
            dskpPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        dskpPaneLayout.setVerticalGroup(
            dskpPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        getContentPane().add(dskpPane, java.awt.BorderLayout.CENTER);

        convMenu.setText("Conversion");

        tempIMenu.setText("Temperatura");
        tempIMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tempIMenuActionPerformed(evt);
            }
        });
        convMenu.add(tempIMenu);

        longIMenu.setText("Longitud");
        convMenu.add(longIMenu);

        pesoIMenu.setText("Peso");
        convMenu.add(pesoIMenu);

        menuBar.add(convMenu);

        contMenu.setText("Contabilidad");

        deprIMenu.setText("Depreciación");

        lineIMenu2.setText("Lineal");
        lineIMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineIMenu2ActionPerformed(evt);
            }
        });
        deprIMenu.add(lineIMenu2);

        SDAaIMenu2.setText("SDA ascendente");
        SDAaIMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SDAaIMenu2ActionPerformed(evt);
            }
        });
        deprIMenu.add(SDAaIMenu2);

        SDAdIMenu2.setText("SDA descendente");
        SDAdIMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SDAdIMenu2ActionPerformed(evt);
            }
        });
        deprIMenu.add(SDAdIMenu2);

        contMenu.add(deprIMenu);

        menuBar.add(contMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tempIMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tempIMenuActionPerformed
        TempIFrame tif = new TempIFrame();
        dskpPane.add(tif);
        tif.setVisible(true);
    }//GEN-LAST:event_tempIMenuActionPerformed

    private void lineIMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineIMenu2ActionPerformed
        DepreIFrame dif = new DepreIFrame(DepreIFrame.DEPR_LINEAL);
        dskpPane.add(dif);
        dif.setVisible(true);
    }//GEN-LAST:event_lineIMenu2ActionPerformed

    private void SDAaIMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SDAaIMenu2ActionPerformed
        DepreIFrame dif = new DepreIFrame(DepreIFrame.DEPR_SDAA);
        dskpPane.add(dif);
        dif.setVisible(true);
    }//GEN-LAST:event_SDAaIMenu2ActionPerformed

    private void SDAdIMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SDAdIMenu2ActionPerformed
        DepreIFrame dif = new DepreIFrame(DepreIFrame.DEPR_SDAD);
        dskpPane.add(dif);
        dif.setVisible(true);
    }//GEN-LAST:event_SDAdIMenu2ActionPerformed

    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            new mainFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem SDAaIMenu2;
    private javax.swing.JMenuItem SDAdIMenu2;
    private javax.swing.JMenu contMenu;
    private javax.swing.JMenu convMenu;
    private javax.swing.JMenu deprIMenu;
    private javax.swing.JDesktopPane dskpPane;
    private javax.swing.JMenuItem lineIMenu2;
    private javax.swing.JMenuItem longIMenu;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem pesoIMenu;
    private javax.swing.JMenuItem tempIMenu;
    // End of variables declaration//GEN-END:variables
}