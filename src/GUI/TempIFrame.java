package GUI;

import java.awt.event.KeyEvent;

public class TempIFrame extends javax.swing.JInternalFrame {
    
    public TempIFrame() {
        initComponents();
    }
    
    private void update(int c, int f, int k){
        cSlider.setValue(c);
        fSlider.setValue(f);
        kSlider.setValue(k);
        cFTF.setText(Integer.toString(c));
        fFTF.setText(Integer.toString(f));
        kFTF.setText(Integer.toString(k));
    }
    
    private int CtoF(int c){
        return (int) ((c * 1.8) + 32);
    }
    
    private int CtoK(int c){
        return c+273;
    }
    
    private int FtoC(int f){
        return (int) ((f - 32) * 0.55);
    }
    
    private int FtoK(int f){
        return (int) (FtoC(f)+273);
    }
    
    private int KtoC(int k){
        return k-273;
    }
    
    private int KtoF(int k){
        return CtoF(KtoC(k));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        cLabel = new javax.swing.JLabel();
        fLabel = new javax.swing.JLabel();
        kLabel = new javax.swing.JLabel();
        cSlider = new javax.swing.JSlider();
        fSlider = new javax.swing.JSlider();
        kSlider = new javax.swing.JSlider();
        cFTF = new javax.swing.JFormattedTextField();
        fFTF = new javax.swing.JFormattedTextField();
        kFTF = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Temperatura");
        setMinimumSize(new java.awt.Dimension(500, 454));
        setNormalBounds(new java.awt.Rectangle(0, 0, 500, 454));
        setPreferredSize(new java.awt.Dimension(500, 454));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        cLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cLabel.setText("Cº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        getContentPane().add(cLabel, gridBagConstraints);

        fLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        fLabel.setText("Fº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        getContentPane().add(fLabel, gridBagConstraints);

        kLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        kLabel.setText("K");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        getContentPane().add(kLabel, gridBagConstraints);

        cSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        cSlider.setPaintTicks(true);
        cSlider.setValue(0);
        cSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                cSliderMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 0);
        getContentPane().add(cSlider, gridBagConstraints);

        fSlider.setMaximum(212);
        fSlider.setMinimum(32);
        fSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        fSlider.setPaintLabels(true);
        fSlider.setPaintTicks(true);
        fSlider.setValue(32);
        fSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                fSliderMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 0);
        getContentPane().add(fSlider, gridBagConstraints);

        kSlider.setMaximum(373);
        kSlider.setMinimum(273);
        kSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        kSlider.setPaintTicks(true);
        kSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                kSliderMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 0);
        getContentPane().add(kSlider, gridBagConstraints);

        cFTF.setText(Integer.toString(cSlider.getValue()));
        cFTF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cFTFKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 6);
        getContentPane().add(cFTF, gridBagConstraints);

        fFTF.setText(Integer.toString(fSlider.getValue())
        );
        fFTF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fFTFKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 6);
        getContentPane().add(fFTF, gridBagConstraints);

        kFTF.setText(Integer.toString(kSlider.getValue())
        );
        kFTF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kFTFKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 6);
        getContentPane().add(kFTF, gridBagConstraints);

        jLabel2.setText("100 Cº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 0);
        getContentPane().add(jLabel2, gridBagConstraints);

        jLabel3.setText("0 Cº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 6, 0);
        getContentPane().add(jLabel3, gridBagConstraints);

        jLabel5.setText("212 Fº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 0);
        getContentPane().add(jLabel5, gridBagConstraints);

        jLabel6.setText("32 Fº");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 6, 0);
        getContentPane().add(jLabel6, gridBagConstraints);

        jLabel8.setText("373 K");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        getContentPane().add(jLabel8, gridBagConstraints);

        jLabel9.setText("273 K");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 6, 0);
        getContentPane().add(jLabel9, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cSliderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cSliderMouseReleased
        int c = cSlider.getValue();
        update(c, CtoF(c), CtoK(c));
    }//GEN-LAST:event_cSliderMouseReleased

    private void fSliderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fSliderMouseReleased
        int f = fSlider.getValue();
        update(FtoC(f), f, FtoK(f));
    }//GEN-LAST:event_fSliderMouseReleased

    private void kSliderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kSliderMouseReleased
        int k = kSlider.getValue();
        update(KtoC(k), KtoF(k), k);
    }//GEN-LAST:event_kSliderMouseReleased

    private void cFTFKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cFTFKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()){
            int c = Integer.parseInt(cFTF.getText());
            update(c, CtoF(c), CtoK(c));
        }
    }//GEN-LAST:event_cFTFKeyPressed

    private void fFTFKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fFTFKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()){
            int f = Integer.parseInt(fFTF.getText());
            update(FtoC(f), f, FtoK(f));
        }
    }//GEN-LAST:event_fFTFKeyPressed

    private void kFTFKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kFTFKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()){
            int k = Integer.parseInt(kFTF.getText());
            update(KtoC(k), KtoF(k), k);
        }
    }//GEN-LAST:event_kFTFKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFormattedTextField cFTF;
    private javax.swing.JLabel cLabel;
    private javax.swing.JSlider cSlider;
    private javax.swing.JFormattedTextField fFTF;
    private javax.swing.JLabel fLabel;
    private javax.swing.JSlider fSlider;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JFormattedTextField kFTF;
    private javax.swing.JLabel kLabel;
    private javax.swing.JSlider kSlider;
    // End of variables declaration//GEN-END:variables
}
