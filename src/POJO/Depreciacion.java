package POJO;

public class Depreciacion {

    private int tipo;
    private float va;
    private float vr;
    private int anos;
    private float[] depr;
    
    public Depreciacion(int tipo) {
        this.tipo = tipo;
    }
    
    public float[] getDepre(float va, float vr, int anos){
        this.va = va;
        this.vr = vr;
        this.anos = anos;
        return setFloat();
    }
    
    private float[] setFloat(){
        this.depr = new float[this.anos];
        for (int i = 0; i < this.anos; i++){
            switch (tipo){
                case 0: {
                    float depre = (this.va-this.vr) / this.anos;
                    this.depr[i] = this.va - (depre*i);  
                }break;
                case 1: {
                    float depre = ((2*(this.anos - i + 1))/(this.anos*(this.anos - 1)))*(this.va - this.vr);
                    this.depr[i] = this.va - depre;
                }break;
                case 2:{
                    
                }break;
            }
        }
        return this.depr;
    }
    
    
}
